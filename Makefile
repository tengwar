all:
	dune build

release:
	dune build --profile=release

test:
	dune runtest src/test

serve:
	cd _build/default && python3 -m http.server 8000
