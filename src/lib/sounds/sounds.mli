module Sig = Sig

type t

val a : t
val e : [`Closed | `Opened] -> t
val eu : [`Closed | `Opened] -> t

val o : t
val schwa: t

(** This is an empty sound.

    It can be used to replace any element in a syllabus *)
val none: t

val space: string -> t

(** This is the voyel i like in "ici"
    When nazalized, the voyel become [in] like in "ainsi" *)
val i : t

(** This is the sound present with letter [ai].
    It is the e like in "semaine".

    When nazalized, the voyel become [in]


*)
val voyel_ai : t

(** This is the sound ou like in "ouvrier"
    When nazalized, the voyel does not change *)
val voyel_u : t

(** This is the sound u like in "unis"
    When nazalized, the voyel become [un] like in "brun" *)
val voyel_y : t

(** Create a diphtongue from a semi-voyel and a voyel. 

    Note that there is no control here that the two elements follows the
    expected type. *)
val diphtongue
  : t -> t -> t

val nasal: t -> t option

val p: t
val b: t
val t: t
val d: t
val k: t
val g: t
val f: t
val v: t
val s: t
val z: t
val sz: t
val ch: t
val j: t

val n: t
val gn: t
val m: t

val r: t
val l: t

val semi_voyel_w: t
val semi_voyel_y: t
val semi_voyel_u: t

val is_voyel : t -> bool
val is_nasal : t -> bool

val is_muted : t -> bool
val mute : t -> t
val unmute: t -> t

val repr
  : (module Sig.REPR) -> t list -> string
