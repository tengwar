module type REPR = sig
  type t

  val space : string -> t

  val none: t

  val a : t
  val a_nasal : t

  val e_opened : t
  val e_closed : t
  val schwa : t

  val eu_closed : t
  val eu_opened : t

  val o : t
  val o_nasal : t

  val i : t
  val i_nasal : t

  val y : t
  val y_nasal : t

  val u : t
  val p : t
  val b : t
  val t : t
  val d : t
  val k : t
  val g : t
  val f : t
  val v : t
  val ch : t
  val j : t
  val s : t
  val z : t
  val m : t
  val n : t
  val gn : t
  val l : t
  val r : t

  val semi_voyel_w : t
  val semi_voyel_y : t
  val semi_voyel_u : t

  val muted : t -> t

  val diphtongue : t -> t -> t

  val fold : t list -> string
end
