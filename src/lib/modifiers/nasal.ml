(* Remove the ending consonant, and transform the voyel into
   the nasal form  *)
let transform
  : Sig.consonants option -> Sounds.t Sig.t -> Sounds.t Sig.t
  = fun c init ->
    let ((v, _) , _) = init in

    begin match Sounds.nasal v with
      | Some t ->
        ( ( t
          , c )
        , None )
      | _ -> init
    end

(** The Nasal modifier transform a voyel followed by N and a consonant
    into a nasal voyel.

    Does this min that nasal voyel are not a distinct element, but just a
    merge from two distinct elements ? *)
let process
  : 'a Sig.modifier
  = fun init ->
    let ((v, c) , ending) = init in
    let ending = Option.bind ending (fun x -> x) in
    let opening = Option.map (fun v -> v.Sig.opening) c in
    let is_voyel = Sounds.is_voyel v in
    match ending, is_voyel, opening with
    | Some ending, _, _ when Sounds.is_nasal ending ->
      transform c init
    | None, false, Some (opening::tl) when Sounds.is_nasal opening ->
      (* If there is no voyel here, transform the opening consonant as an
         ending consonant for the next syllabus *)
      let c = Option.map
          (fun c ->
             { c with
               Sig.opening = tl
             ; Sig.ending = (Some (Some opening))
             }) c in
      ( ( v
        , c )
      , None )
    | _ ->
      (* Return the element unchanged *)
      init

