open StdLabels

(** Mute the last consonant if there is no voyel in the syllabe.

    This modifier is only applied in the first step, and not repeated anymore.
*)
let process
  : 'a Sig.modifier
  = fun init ->
    let ((v, c) , ending) = init in
    let is_voyel = Sounds.is_voyel v in
    match is_voyel, c, ending with
    | false, Some c, None ->
      let c = { c with Sig.opening = List.map ~f:Sounds.mute c.Sig.opening } in
      ((v, Some c) , ending)
    | _ -> init


