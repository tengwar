(** Transform the ending e.

    For example "ie$" into "i"

*)
let ending_e
  : 'a Sig.modifier
  = fun init ->
    let ((v, c) , ending) = init in

    (*
    if v = Sounds.diphtongue Sounds.semi_voyel_y Sounds.schwa then
      ((Sounds.i, c), ending)
    else
        *)
    if v = Sounds.schwa then (
      match c, ending with
      (* If there is no consonant, and just a final e, remove it *)
      | None, None -> ((Sounds.none, c), ending)
      | _ -> init
    ) else
      init

(** Transform the e into eu or E *)
let process
  : 'a Sig.modifier
  = fun init ->
    let ((v, c) , ending) = init in

    match ending with
    | Some _ when v = Sounds.schwa ->
      (* If there is an ending consonant, change the e into E like essai *)
      ((Sounds.e `Opened, c) , ending)
    | _ -> ((v, c) , ending)

