type voyel = Sounds.t

type consonants =
  { ending : Sounds.t option option
  ; opening : Sounds.t list
  ; following : Sounds.t option }

type 'a group = voyel * consonants option

type 'a t = 'a group * 'a option option

type 'a modifier = Sounds.t t  -> Sounds.t t
