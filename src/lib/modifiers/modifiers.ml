module Sig = Sig

let nasal = Nasal.process
let vocalize_s = Vocalize.process
let mute_consonant = Mute.process
let e = E.process
let ending_e = E.ending_e
