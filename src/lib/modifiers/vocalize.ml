(** Transform the S into Z if the S is the opening consonant and
    there is no ending consonant before *)
let process
  : 'a Sig.modifier
  = fun init ->
    let ((v, c) , ending) = init in

    match c with
    | None -> init
    | Some op ->
      (* The voyel may be none in case of ending word. In such case, we shall
         not transform the S into Z *)
      let is_voyel = Sounds.is_voyel v in

      match is_voyel, op.Sig.opening, op.Sig.ending with
      | true, hd::[], None when hd = Sounds.sz ->
        let c = Some { op with opening = [Sounds.z] } in
        ((v, c) , ending)
      | true, hd::n::[], None when hd = Sounds.sz && Sounds.is_voyel n->
        (* The s is followed by a semi-voyel *)
        let c = Some { op with opening = [Sounds.z; n] } in
        ((v, c) , ending)
      | _ -> init
