type position_type =
  [ `Above
  | `Below ]

(** Consonant category *)
type category

type 'a voyel =
  { position : position_type
  ; app : ('a -> string) }

type 'a consonant =
  { position : 'a
  ; muted: string option
  ; repr : string
  ; primary : bool
  ; category : category }

type 'a glyph =
  { tengwa : 'a consonant option
  (* The eventual Tehta above the tengwa *)
  ; tehta_above : 'a voyel option
  (* And below *)
  ; tehta_below : 'a voyel option
  }


val empty_glyph : 'a glyph
val portant: string -> 'a -> 'a consonant

type 'a t =
  | Consonant: 'a consonant -> 'a t
  | Voyel: 'a voyel -> 'a t
  | Application: ('a t -> 'a t list) -> 'a t
  | Repr of string
  | Glyph: 'a glyph -> 'a t
  | Nothing

val voyel
  : position_type -> ('a -> string) -> 'a t

val none : 'a t
val space :  string -> 'a t

val nasal 
  : 'a t -> 'a t -> 'a t -> ' a voyel -> 'a t

val diphtongue
  : 'a t -> 'a t -> 'a t

val fold
  : portant:'a consonant -> 'a t list -> string

val p: string -> string option -> 'a -> 'a t
val b: string -> string option -> 'a -> 'a t
val t: string -> string option -> 'a -> 'a t
val d: string -> string option -> 'a -> 'a t
val k: string -> string option -> 'a -> 'a t
val g: string -> string option -> 'a -> 'a t
val f: string -> 'a -> 'a t
val v: string -> 'a -> 'a t
val ch: string -> 'a -> 'a t
val j: string -> 'a -> 'a t
val s: string -> string option -> 'a -> 'a t
val z: string -> 'a -> 'a t
val m: string -> 'a -> 'a t
val n: string -> 'a -> 'a t
val gn: string -> 'a -> 'a t
val ng: string -> 'a -> 'a t
val r: string -> string option -> 'a -> 'a t
val l: string -> 'a -> 'a t
val semi_voyel_u: string -> 'a -> 'a t
val semi_voyel_y: string -> 'a -> 'a t
val semi_voyel_w: string -> 'a -> 'a t
