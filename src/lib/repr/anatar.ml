module T = Rousseau

(** Glyph position for the diacritc *)
type position =
  | Pos1
  | Pos2
  | Pos3
  | Pos4
  | Lambe       (* Position for the Lambe (Theta inside) *)
  | Lower_1_2   (* Position for the Harma *)
  | Lower_3_1   (* Position for the Calma *)
  | Lower_4_2   (* Position for the Thule *)

type t = position T.t

let none = T.none

let space s = T.space s

let a' =
  { T.position = `Above
  ; T.app = function
      | Pos1 | Lower_1_2 | Lambe -> "#"
      | Pos2 -> "E"
      | Pos3 | Lower_3_1 -> "D"
      | Pos4 | Lower_4_2 -> "C"
  }
let a = T.Voyel a'

let app_e = function
  | Pos1 | Lower_1_2 | Lambe -> "$"
  | Pos2 -> "R"
  | Pos3 | Lower_3_1 -> "F"
  | Pos4 | Lower_4_2 -> "V"

and app_eu = function
  | Pos1 | Lower_1_2 | Lambe -> "Ü"
  | Pos2 -> "Ý"
  | Pos3 | Lower_3_1 -> "Þ"
  | Pos4 | Lower_4_2 -> "ß"

let e_opened =
  T.Voyel { position = `Above ; app = app_e }
and e_closed =
  T.Voyel { position = `Above ; app = app_e }

and schwa = T.Voyel
    { position = `Below
    ; app = function
        | Pos1 | Lower_3_1 -> "È"
        | Pos2 | Lower_1_2 | Lower_4_2 -> "É"
        | Pos3 -> "Ê"
        | Pos4 -> "Ë"
        | Lambe -> "L"
    }

and eu_opened = T.Voyel { position = `Above ; app = app_eu }
and eu_closed = T.Voyel { position = `Above ; app = app_eu }

and o' =
  { T.position = `Above
  ; T.app = function
      | Pos1 | Lower_1_2 | Lambe -> "^"
      | Pos2 -> "Y"
      | Pos3 | Lower_3_1 -> "H"
      | Pos4 | Lower_4_2 -> "N"
  }

let o = T.Voyel o'

and i' =
  { T.position = `Above
  ; T.app = function
      | Pos1 | Lower_1_2 | Lambe -> "%"
      | Pos2 -> "T"
      | Pos3 | Lower_3_1 -> "G"
      | Pos4 | Lower_4_2 -> "B"
  }

let i = T.Voyel i'

and y' =
  { T.position = `Above
  ; T.app = function
      | Pos1 | Lower_1_2 | Lambe -> "Ø"
      | Pos2 -> "Ù"
      | Pos3 | Lower_3_1 -> "Ú"
      | Pos4 | Lower_4_2 -> "Û"
  }

let y = T.Voyel y'

and u = T.Voyel
    { T.position = `Above
    ; T.app = function
        | Pos1 | Lower_1_2 | Lambe -> "&"
        | Pos2 -> "U"
        | Pos3 | Lower_3_1 -> "J"
        | Pos4 | Lower_4_2 -> "M"
    }

let portant =       T.portant       "`"             Pos4

and t =             T.t             "1" (Some "6")  Pos2
and d =             T.d             "2" (Some "6")  Pos1
and p =             T.p             "q" (Some "y")  Pos2
and b =             T.b             "w" (Some "y")  Pos1
and k =             T.k             "a" (Some "h")  Lower_3_1
and g =             T.g             "s" (Some "h")  Pos1
and f =             T.f             "e"             Pos3
and v =             T.v             "r"             Pos1
and ch =            T.ch            "d"             Lower_1_2
and j =             T.j             "f"             Pos1
and s =             T.s             "3" (Some "i")  Lower_4_2
and z =             T.z             "4"             Pos1
and m =             T.m             "t"             Pos1
and n =             T.n             "5"             Pos1
and gn =            T.gn            "b"             Pos1
and ng =            T.ng            "g"             Pos1
and r =             T.r             "7" (Some "u")  Pos2
and l =             T.l             "j"             Lambe

and semi_voyel_w =  T.semi_voyel_w  "."             Pos3
and semi_voyel_y =  T.semi_voyel_y  "l"             Pos1
and semi_voyel_u =  T.semi_voyel_u  "]"             Pos2

let a_nasal =       T.nasal m ng n a'
and o_nasal =       T.nasal m ng n o'
and i_nasal =       T.nasal m ng n i'
and y_nasal =       T.nasal m ng n y'

let muted
  : t -> t
  = fun t ->
    match t with
    | Consonant c ->
      begin match c.muted with
        | None -> t
        | Some s -> Consonant {c with repr = s ; position = Pos2}
      end
    | _ -> t

let diphtongue = T.diphtongue

let fold = T.fold ~portant
