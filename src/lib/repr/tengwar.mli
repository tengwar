type tehta

val three_dots : tehta
val two_dots   : tehta
val dot        : tehta
val acute      : tehta
val agrave     : tehta
val right_curl : tehta 
val left_curl  : tehta 
val double_right_curl: tehta 
val double_left_curl:  tehta 
val double_acute: tehta
val bar: tehta

type tengwa =
  | Tinco
  | Parma
  | Calma
  | Quesse

  | Ando
  | Umbar
  | Anga
  | Ungwe

  | Sule
  | Formen
  | Harma
  | Hwesta

  | Anto
  | Ampa
  | Anca
  | Unque

  | Numen
  | Malta
  | Noldo
  | Nwame

  | Ore
  | Vala
  | Anna
  | Vilya

  | Romen
  | Arda
  | Lambe
  | Alda

  | Silme
  | Silme_nuquerna
  | Esse
  | Esse_nuquerna

  | Hyarmen
  | Hwesta_sindarinwa
  | Yanta
  | Ure

  | Halla
  | Osse
  | Telco
  | Ara

type t =
  { tengwa : tengwa
  ; tehta_above : tehta option
  ; tehta_below : tehta option
  }

val build
  : tengwa -> tehta option -> tehta option -> t
