type tengwa =
  | Tinco
  | Parma
  | Calma
  | Quesse

  | Ando
  | Umbar
  | Anga
  | Ungwe

  | Sule
  | Formen
  | Harma
  | Hwesta

  | Anto
  | Ampa
  | Anca
  | Unque

  | Numen
  | Malta
  | Noldo
  | Nwame

  | Ore
  | Vala
  | Anna
  | Vilya

  | Romen
  | Arda
  | Lambe
  | Alda

  | Silme
  | Silme_nuquerna
  | Esse
  | Esse_nuquerna

  | Hyarmen
  | Hwesta_sindarinwa
  | Yanta
  | Ure

  | Halla
  | Osse
  | Telco
  | Ara

type tehta =
  | Three_dots
  | Two_dots
  | Dot
  | Acute
  | Agrave
  | Double_Acute
  | Right_curl
  | Left_curl
  | Double_Right_curl
  | Double_Left_curl
  | Bar


let three_dots = Three_dots
let two_dots = Two_dots
let dot = Dot
let acute = Acute
let agrave = Agrave
let right_curl = Right_curl
let left_curl = Left_curl
let double_right_curl = Double_Right_curl
let double_left_curl = Double_Left_curl
let double_acute = Double_Acute
let bar = Bar

type t =
  { tengwa : tengwa
  ; tehta_above : tehta option
  ; tehta_below : tehta option
  }

let build
  : tengwa -> tehta option -> tehta option -> t
  = fun tengwa a b ->

    { tengwa
    ; tehta_below = b
    ; tehta_above = a }

