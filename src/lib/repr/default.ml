open StdLabels

type t = string

let none = ""

let space s = s

let a = "a"
and a_nasal = "@"

and e_opened = "E"
and e_closed = "e"
and schwa = "°"

and eu_opened = "9"
and eu_closed = "2"

and o = "o"
and o_nasal = "§"

and i = "i"
and i_nasal = "5"
and y = "y"
and y_nasal = "1"

and u = "u"

and p = "p"
and b = "b"
and t = "t"
and d = "d"

and k = "k"
and g = "g"

and f = "f"
and v = "v"

and ch = "S"
and j = "Z"

and s = "s"
and z = "z"

and m = "m"
and n = "n"

and gn = "N"

and l = "L"
and r = "R"

and semi_voyel_w = "w"
and semi_voyel_y = "j"
and semi_voyel_u = "8"

let muted
  : t -> t
  = fun t ->
    Printf.sprintf "(%s)" t

let diphtongue
  : t -> t -> t
  = fun t1 t2 ->
    Printf.sprintf "[%s%s]" t1 t2

let fold
  : t list -> string
  = fun elems ->
    let buff = Buffer.create 16 in
    List.iter elems
      ~f:(fun f -> Buffer.add_string buff f);
    Buffer.contents buff
