module T = Rousseau

(** Glyph position for the diacritc *)
type position = unit
type t = position T.t

let none = T.none

let space s = T.space s

let a' =
  { T.position = `Above
  ; T.app = fun () -> ""
  }
let a = T.voyel `Above (fun () -> "")

let app_e = fun () -> ""
and app_eu = fun () -> ""

let e_opened =
  T.Voyel { position = `Above ; app = app_e }
and e_closed =
  T.Voyel { position = `Above ; app = app_e }

and schwa = T.Voyel
    { position = `Below
    ; app = fun () -> ""
    }

and eu_opened = T.Voyel { position = `Above ; app = app_eu }
and eu_closed = T.Voyel { position = `Above ; app = app_eu }

and o' =
  { T.position = `Above
  ; T.app = fun () -> ""
  }

let o = T.Voyel o'

and i' =
  { T.position = `Above
  ; T.app = fun () -> ""
  }

let i = T.Voyel i'

and y' =
  { T.position = `Above
  ; T.app = fun () -> ""
  }

let y = T.Voyel y'

and u = T.Voyel
    { T.position = `Above
    ; T.app = fun () -> ""
    }

let portant =       T.portant       ""             ()

and t =             T.t             "" (Some "")  ()
and d =             T.d             "" (Some "")  ()
and p =             T.p             "" (Some "")  ()
and b =             T.b             "" (Some "")  ()
and k =             T.k             "" (Some "")  ()
and g =             T.g             "" (Some "")  ()
and f =             T.f             ""             ()
and v =             T.v             ""             ()
and ch =            T.ch            ""             ()
and j =             T.j             ""             ()
and s =             T.s             "" (Some "")  ()
and z =             T.z             ""             ()
and m =             T.m             ""             ()
and n =             T.n             ""             ()
and gn =            T.gn            ""             ()
and ng =            T.ng            ""             ()
and r =             T.r             "" (Some "")  ()
and l =             T.l             ""             ()

and semi_voyel_w =  T.semi_voyel_w  ""             ()
and semi_voyel_y =  T.semi_voyel_y  ""             ()
and semi_voyel_u =  T.semi_voyel_u  ""             ()

let a_nasal =       T.nasal m ng n a'
and o_nasal =       T.nasal m ng n o'
and i_nasal =       T.nasal m ng n i'
and y_nasal =       T.nasal m ng n y'

let muted
  : t -> t
  = fun t ->
    match t with
    | Consonant c ->
      begin match c.muted with
        | None -> t
        | Some s -> Consonant {c with repr = s ; position = ()}
      end
    | _ -> t

let diphtongue = T.diphtongue

let fold = T.fold ~portant

