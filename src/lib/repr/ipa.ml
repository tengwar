open StdLabels

type t = string

let none = ""

let space s = s

let a = "a"
and a_nasal = "ɑ̃"

and e_opened = "ɛ"
and e_closed = "e"
and schwa = "ə"

and eu_opened = "œ"
and eu_closed = "ø"

and o = "o"
and o_nasal = "ɔ̃"

and i = "i"
and i_nasal = "ɛ̃"
and y = "y"
and y_nasal = "œ̃"

and u = "y"

and p = "p"
and b = "b"
and t = "t"
and d = "d"

and k = "k"
and g = "g"

and f = "f"
and v = "v"

and ch = "ʃ"
and j = "ʒ"

and s = "s"
and z = "z"

and m = "m"
and n = "n"

and gn = "ɲ"

and l = "l"
and r = "ʁ"

and semi_voyel_w = "w"
and semi_voyel_y = "j"
and semi_voyel_u = "ɥ"

let muted
  : t -> t
  = fun t ->
    Printf.sprintf "(%s)" t

let diphtongue
  : t -> t -> t
  = fun t1 t2 ->
    Printf.sprintf "[%s%s]" t1 t2

let fold
  : t list -> string
  = fun elems ->
    let buff = Buffer.create 16 in
    List.iter elems
      ~f:(fun f -> Buffer.add_string buff f);
    Buffer.contents buff
