{

 open Prononciation

 exception Error of string

}

let ending = eof | '\n'

rule letter = parse
| '|'            { Sep }
| 'a'            { A }
| 'b'            { B }
| 'c'            { C }
| 'd'            { D }
| 'e'            { E Accent.NONE }
| '\232'         { E Accent.ACUTE }
| "è"            { E Accent.AGRAVE }
| "ê"            { E Accent.AGRAVE }
| '\233'         { E Accent.ACUTE }
| "é"            { E Accent.ACUTE }
| 'f'            { F }
| 'g'            { G }
| 'h'            { H }
| 'i'            { I }
| 'j'            { J }
| 'k'            { K }
| 'l'            { L }
| 'm'            { M }
| "mm"           { M }
| 'n'            { N }
| "nn"           { N }
| 'o'            { O }
| 'p'            { P }
| "ph"           { F }
| 'q'            { Q }
| 'r'            { R }
| 'u'            { U }
| 's'            { S }
| 't'            { T }
| 'u'            { U }
| 'v'            { V }
| 'w'            { W }
| 'x'            { X }
| 'y'            { Y }
| 'z'            { Z }
| ending         { EOL }
| "er" ending    { ER_ }
| "erf" ending   { ERF_ }
| "el" ending    { EL_ }
(*| "ent" ending   { ENT_ }*)
| "ient" ending  { IENT_ }
| "ie" ending    { IE_ }
| "x" ending     { X_ }
| _              { Space (Lexing.lexeme lexbuf)}

(* This rule looks for a single line, terminated with '\n' or eof.
   It returns a pair of an optional string (the line that was found)
   and a Boolean flag (false if eof was reached). *)

and line = parse
| ([^'\n']* '\n') as line
    (* Normal case: one line, no eof. *)
    { Some line, true }
| eof
    (* Normal case: no data, eof. *)
    { None, false }
| ([^'\n']+ as line) eof
    (* Special case: some data but missing '\n', then eof.
       Consider this as the last line, and add the missing '\n'. *)
    { Some (line), false }
