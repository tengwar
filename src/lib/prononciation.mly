%{

    (** This module transform the words into a semi-prononciation elements. 

    The letters should be quite close with the associate phonem, but some
    modification can occur.

     *)

    open Tokens

    let letter_e = function
        | Accent.NONE   -> Tokens.E
        | Accent.AGRAVE -> Tokens.E_AGRAVE
        | Accent.ACUTE  -> Tokens.E_ACUTE

%}

%token X_
%token ENT_
%token ERF_
%token ER_
%token EL_
%token IENT_
%token IE_
%token Sep

%token A
%token B
%token C
%token D
%token<Accent.t> E
%token F
%token G
%token H
%token I
%token J
%token K
%token L
%token M
%token N
%token O
%token Q
%token P
%token R
%token S
%token T
%token U
%token V
%token W (* semi voyel w *)
%token X
%token Y (* semi voyel j *)
%token Z
%token <string>Space
%token EOL

%nonassoc Low
%right High

%start<Tokens.token list> main
%%

voyel_without_i
  : A           { A }
  | E           { letter_e $1 }
  | O           { O }
  | A U         { O }
  | E A U       { O }
  | O U         { OU }
  | U           { U }

voyel
  : I           { I }
  | voyel_without_i { $1 }


letters
  : voyel  { $1 :: [] }
  | voyel_without_i I { $1 :: I :: []} 
  | Space   { (Space $1) :: [] }
  | Sep     { Sep :: [] }

  | B       { B :: [] }
  | C       { K :: [] }
  | C H     { X :: [] }
  | C I     { S :: I :: [] }
  | C E     { S :: letter_e $2 :: [] }
  | C U     { K :: U :: [] }
  | C U E   { K :: E :: [] }
  | D       { D :: [] }
  | D D     { D :: [] }
  | F       { F :: [] }
  | F F     { Nothing :: F :: [] }
  | G       { G :: [] }
  | G I     { J :: I :: [] }
  | G E     { J :: letter_e $2 :: [] }
  | G E voyel { J :: $3 :: [] }
  | G U     { G :: U :: [] }
  | G U I   { G :: I :: [] }
  | G U E   { G :: letter_e $3 :: [] }
  | H       { Sep :: [] }

  | J       { J :: [] }
  | K       { K :: [] }
  | L L     { Nothing :: L :: [] }
  | I L     { I :: L :: [] }
  | I L L   { I :: Y :: [] }
  | voyel_without_i I L L   { $1 :: Y :: [] }
  | L       { L :: [] }
  | M       { M :: [] }
  | M M     { M :: [] }
  | N       { N :: [] }
  | N N     { N :: [] }

  | O I     { W :: A :: [] }
  | O IE_   { W :: A :: [] }
  | O IENT_ { W :: A :: [] }
  | O I N   { W :: I :: N :: [] }

  | P P     { Nothing :: P :: [] }
  | P       { P :: [] }

  | Q       { K :: [] }
  | Q U     { K :: [] }

  | R       { R :: [] }
  | R R     { Nothing :: R :: [] }
  | S       { SZ :: [] }
  | S S     { Nothing :: S :: [] }
  | S H     { X :: [] }
  | T       { T :: [] }
  | T T     { Nothing :: T :: [] }

  | V       { V :: [] }
  | W       { W :: [] }
  | X       { K :: Sep :: SZ :: [] }
  | Y       { Y :: [] }

  | Z       { Z :: [] }

ending: 
  | X_        { S::EOL::[]}
  | IENT_     { I::T::EOL::[]}
  | IE_       { I::EOL::[]}
  | ER_       { E_ACUTE::R::EOL::[]}
  | ERF_      { E_AGRAVE::R::EOL::[]}
  | EL_       { E_AGRAVE::L::EOL::[]}
  | EOL       { EOL::[] }

main: 
  | append(flatten(letters*), ending)     { $1 }
