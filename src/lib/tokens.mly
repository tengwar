%token Sep

%token Nothing
%token A
%token B
%token D
%token E
%token E_ACUTE
%token E_AGRAVE
%token F
%token G
%token I
%token J
%token K
%token L
%token M
%token N
%token O
%token OU
%token P
%token R
%token S
%token SZ
%token T
%token U
%token V
%token W (* semi voyel w *)
%token X
%token Y (* semi voyel j *)
%token Z
%token <string>Space
%token EOL

%nonassoc Low
%right High

%%
