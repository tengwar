module T = Translator

let process (optional_line : string option) =
  match optional_line with
  | None -> ()
  | Some line ->

    let result = T.Reader.process line in

    let res1 = Result.map
        (fun t-> Sounds.repr (module Repr.Default) t)
        result in
    let () = match res1 with
      | Ok response -> print_endline response
      | Error err -> print_endline err in
    let res2 = Result.map
        (fun t-> Sounds.repr (module Repr.Anatar) t)
        result in
    let () = match res2 with
      | Ok response -> print_endline ( "Anatar : " ^ response)
      | Error err -> print_endline err in
    let res3 = Result.map
        (fun t-> Sounds.repr (module Repr.Telcontar) t)
        result in
    let () = match res3 with
      | Ok response -> print_endline ( "Telcontar : " ^ response)
      | Error err -> print_endline err in
    ()

let rec repeat channel =
  (* Attempt to read one line. *)
  let optional_line, continue = T.Lexer.line channel in
  process optional_line;
  if continue then
    repeat channel

let () =
  repeat (Lexing.from_channel stdin)
