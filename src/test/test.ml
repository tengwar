open StdLabels

let error = ref 0

let process (optional_line : string option) expected =
  match optional_line with
  | None -> ()
  | Some line ->
    match Translator.Reader.process line with
    | exception _ ->
      error := 1;
      Printf.fprintf stdout
        "%s : error \n%!"
        line
    | Error result ->
      error := 1;
      Printf.fprintf stdout
        "%s : %s\n%!"
        line
        result
    | Ok response ->
      let response = Sounds.repr (module Repr.Default) response in
      if String.equal response expected then
        ()
            (*
        (Printf.fprintf stdout "%s : %s OK \n%!"
           line
           expected)
           *)
      else (
        error := 1;
        (Printf.fprintf stdout "%s : got %s / %s expected\n%!"
           line
           response
           expected)
      )


let rec repeat input expected =
  (* Attempt to read one line. *)
  let optional_line, continue = Translator.Lexer.line input in
  process optional_line expected;
  if continue then
    repeat input expected

let tests =
  [ "abaca",        "abaka"
  ; "abaissa",      "abEsa"
  ; "abaissait",    "abEsE(t)"
  ; "abaissant",    "abEs@(t)"
  ; "abaissées",    "abEse(s)"
  ; "abaissera",    "abEs°Ra"
  ; "achat",        "aSa(t)"
  ; "agneau",       "aNo"
  ; "aimes",        "Em°(s)"
  ; "anniversaire", "anivERsER°"
  ; "anta",         "@ta"
  ; "anneaux",      "ano(s)"
  ; "arachide",     "aRaSid°"
  ; "as",           "a(s)"
  ; "asia",         "az[ja]"
  ; "astiqué",      "astike"
  ; "atmosphère",   "atmosfER°"
  ; "automne",      "ot§n°"
  ; "autruche",     "otRyS°"
  ; "besoin",       "b°z[w5]"
  ; "beaumont",     "bom§(t)"
  ; "bisoux",       "bizu(s)"
  ; "boulangerie",  "buL@Z°Ri"
  ; "casait",       "kazE(t)"
  ; "cassait",      "kasE(t)"
  ; "célibat",      "seLiba(t)"
  ; "chanci",       "S@si"
  ; "chat",         "Sa(t)"
  ; "chipant",      "Sip@(t)"
  ; "crions",       "kRi§(s)"
  ; "co|incidant",  "ko5sid@(t)"
  ; "croire",       "kR[wa]R°"
  ; "cuillère",     "k[8i][jE]R°"
  ; "demeure",      "d°m9R°"
  ; "diag|nostic",  "d[ja]gnostik"
  ; "ébrouas",      "ebRua(s)"
  ; "effroi",       "EfR[wa]"
  ; "em|magasinais","@magazinE(s)"
  ; "essai",        "EsE"
  ; "extra",        "EkstRa"
  ; "famille",      "fami[j°]"
  ; "feuille",      "f9[j°]"
  ; "final",        "finaL"
  ; "liant",        "L[j@](t)"
  ; "lion",         "L[j§]"
  ; "loin",         "L[w5]"
  ; "lui",          "L[8i]"
  ; "groin",        "gR[w5]"
  ; "hélicoptère",  "eLikoptER°"
  ; "hirondelle",   "iR§dEL°"
  ; "jama|iquain",  "Zamaik5"
  ; "joues",        "Zu(s)"
  ; "libellule",    "LibELyL°"
  ; "main",         "m5"
  ; "manger",       "m@ge(R)"
  ; "merci",        "mERsi"
  ; "ménageais",    "menaZE(s)"
  ; "mouillage",    "mu[ja]Z°"
  ; "neige",        "nEZ°"
  ; "neuf",         "n9f"
  ; "nerf",         "nE(R)"
  ; "pacha",        "paSa"
  ; "péché",        "peSe"
  ; "persai",       "pERsE"
  ; "personne",     "pERson°"
  ; "plan",         "pL@"
  ; "plat",         "pLa(t)"
  ; "plein",        "pL5"
  ; "platte",       "pLat°"
  ; "proie",        "pR[wa]"
  ; "quille",       "ki[j°]"
  ; "rébellion",    "RebEL[j§]"
  ; "reine",        "REn°"
  ; "rien",         "R[j5]"
  ; "soin",         "s[w5]"
  ; "souris",       "suRi(s)"
  ; "toiture",      "t[wa]tyR°"
  ; "trois",        "tR[wa](s)"
  ; "vieux",        "v[j9](s)"
  ; "vil|le",       "viLL°"
  ; "wèb",          "[wE]b"
  ]

let () =
  let n = ref 0 in
  let () = List.iter tests
      ~f:(fun (input, expected) ->
          incr n;
          repeat (Lexing.from_string input) expected)
  in

  Printf.printf "Runned %d tests. Got %d errors\n" (!n) (!error);
  exit !error

